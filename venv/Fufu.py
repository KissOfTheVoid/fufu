import itertools
import matplotlib.pyplot as plt
import math
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error, confusion_matrix, roc_auc_score, roc_curve
from sklearn.linear_model import LogisticRegression
import scikitplot as skplt
from sklearn.ensemble import RandomForestClassifier
import scipy
from jcamp import JCAMP_reader


def make_frame(jdx_file, podg=0.4):
    '''
    Делает из jdx DataFrame
    :param jdx_file: jdx файл
    :param podg: коэффициент масшатбирования
    :return: DataFrame
    '''
    jcamp_dict_jdx_file = JCAMP_reader(jdx_file)
    frame = pd.DataFrame(jcamp_dict_jdx_file)
    frame['y'] = frame['y'] ** podg
    return frame


def plot_spectre(jcamp_dict_meth, scale_1=1):
    '''
    Строи спектр
    :param jcamp_dict_meth: Dataframe с данными
    :return: Строит график
    '''
    plt.plot(jcamp_dict_meth['x'], jcamp_dict_meth['y'] ** scale_1)
    plt.title('IR_Spectrum')
    plt.xlabel('1/CM')
    plt.ylabel('Transmittence')
    plt.show()


def plot_spectre_with_noise(jcamp_dict_meth, noise):
    '''
    Строи спектр c шумом
    :param jcamp_dict_meth: DataFrame с исходным спектром
    :param noise: Столбец шума
    :return:
    '''
    jcamp_dict_meth['y'] = jcamp_dict_meth['y'] + noise
    plt.clf()
    plt.plot(jcamp_dict_meth['x'], jcamp_dict_meth['y'])
    plt.title('IR_Spectre')
    plt.xlabel('1/CM')
    plt.ylabel('Transmittence')
    plt.show()


def generate_noise(df, ratio=5):
    '''
    Генерирует шум с SNR=ratio
    :param df: Столбец пропускания
    :param ratio: SNR
    :return:
    '''
    intensity = df.std()
    scale = math.sqrt(intensity ** 2 / ratio)
    size = df.shape[0]
    rand = np.random.normal(loc=0.0, scale=scale, size=size)
    noise = pd.Series(rand, dtype='float64')
    return noise


def add_noise(st, ratio=5):
    '''
    Создает экпериментальный спектр
    :param st:
    :param noi:
    :param ratio:
    :return:
    '''
    noi=generate_noise(st, ratio)
    st_noi = st+noi
    return st_noi


def calculate_snr_mnk_trash(spectr, trash=0.01):
    for r in list(map(lambda x: x/10.0, range(1, 100, 1))):
        delta = mean_squared_error(spectr['y'], add_noise(spectr['y'], ratio=r))
        if delta < trash:
            print('r=', r)
            print(delta)
            break


def calculate_snr_mnk(spectr, noise):
        delta = mean_squared_error(spectr['y'], noise)
        print(delta)


def calculate_snr_pirs(spectr, trash=0.01):
    delta_min = 0
    for r in list(map(lambda x: x/10.0, range(1, 100, 1))):
        delta = scipy.stats.pearsonr(np.array(spectr['y']), np.array(add_noise(spectr['y'], ratio=r)))[0]
        if delta > delta_min:
            delta_min=delta
            print(delta_min)
            print('r_min=', r)


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")


def generate_matrix(df1,df2):
    matrix = confusion_matrix(np.array(df1), np.array(df2))
    plot_confusion_matrix(matrix, classes=['1', '2'])
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()


def generate_true_samples(spectr1, spectr2, spectr3, n=1000):
    d = np.ones((n-1, 5))
    for i in range(1, n):
        print(i)
        d[i-1][1] = scipy.stats.pearsonr(np.array(spectr1['y']), np.array(add_noise(spectr1['y'], ratio=5)))[0]
        d[i - 1][2] = scipy.stats.pearsonr(np.array(spectr2['y']), np.array(add_noise(spectr2['y'], ratio=5)))[0]
        d[i - 1][3] = scipy.stats.pearsonr(np.array(spectr3['y']), np.array(add_noise(spectr3['y'], ratio=5)))[0]
    df = pd.DataFrame(data=d, columns=['target', 'meth', 'acr', 'amm', 'true'])
    mid = (df['meth'].max() - df['meth'].min()) / 2
    df['target'][df['meth'] < (df['meth'].min() + mid)] = '-1.0'
    df['target'][(df['meth'] < df['acr']) & (df['meth'] < df['amm'])] = '-1.0'
    df['true'][(df['meth'] < df['acr']) & (df['meth'] < df['amm'])] = '-1.0'
    df['true'][df['meth'] < (df['meth'].min() + mid * 0.5)] = '-1.0'

    return df


def generate_test_samples(spectr, n=10000):
    for i in range(1, n):
        d = np.ones(n-1)
        d[i - 1] = scipy.stats.pearsonr(np.array(spectr['y']), np.array(add_noise(spectr['y'], ratio=5)))[0]
        df1 = pd.DataFrame(data=d, columns=['y'])
    return df1


def plot_roc_curve(y_true,y_pred):
    fpr, tpr, _ = roc_curve(y_true, y_pred)
    auc = roc_auc_score(y_true, y_pred)
    plt.plot(fpr, tpr, label="data 1, auc=" + str(auc))
    plt.legend(loc=4)
    plt.show()


def create_test_frame(ir_1, ir_2, ir_3, ir_4, ir_5, ir_6, ir_7, n_samples=100):
    '''
    Создает базу экспериментальных данных
    :param ir_1:
    :param ir_2:
    :param ir_3:
    :param ir_4:
    :param ir_5:
    :param ir_6:
    :param ir_7:
    :param n_samples:
    :return:
    '''
    d = np.ones((n_samples*7, 2))
    i = 0
    for r in list(map(lambda x: x / (n_samples/2), range(1, n_samples + 1, 1))):
        d[i][0] = scipy.stats.pearsonr(np.array(ir_1['y']), np.array(add_noise(ir_1['y'], ratio=r)))[0]
        d[i][1] = 1
        i = i + 1

    ir_list = [ir_2, ir_3, ir_4, ir_5, ir_6, ir_7]
    for ir in ir_list:
        remove_n = np.abs(ir_1['y'].shape[0]-ir['y'].shape[0])
        print(ir_1['y'].shape[0]-ir['y'].shape[0])
        if np.sign(ir_1['y'].shape[0]-ir['y'].shape[0]) == 1:
            print(1)
            drop_indices = np.random.choice(ir_1.index, remove_n, replace=False)
            ir_0 = ir_1.drop(drop_indices)
            for r in list(map(lambda x: x / (n_samples / 16), range(1, n_samples+1, 1))):
                d[i][0] = scipy.stats.pearsonr(np.array(ir_0['y']), np.array(add_noise(ir['y'], ratio=r)))[0]
                d[i][1] = 0
                i = i + 1
        if np.sign(ir['y'].shape[0] - ir_1['y'].shape[0]) == 1:
            print(-1)
            drop_indices = np.random.choice(ir.index, remove_n, replace=False)
            for r in list(map(lambda x: x / (n_samples / 16), range(1, n_samples + 1, 1))):
                ir_with_n = add_noise(ir['y'], ratio=r)
                ir_with_n = ir_with_n .drop(drop_indices)
                d[i][0] = scipy.stats.pearsonr(np.array(ir_1['y']), np.array(ir_with_n))[0]
                d[i][1] = 0
                i = i + 1

    return d


def make_a_mix_of_three(ir_1, ir_2, ir_3, c_1=1, c_2=0.2, c_3=0.4):
    mix = pd.DataFrame(data=[(1 - (1 - ir_1['y'] ** c_1) + (1 - ir_2['y'] ** c_2 )+ (1 - ir_3['y'] ** c_3))])
    mix1 = pd.DataFrame(data=mix.transpose())
    mix1 = mix1.dropna()
    return mix1


np.random.seed(10)
meth = make_frame('Methanol.jdx')  # 1
ace = make_frame('Acetone.jdx')  # 2
acr = make_frame('Acrolein.jdx')  # 3
tric = make_frame('Trichloronitromethane.jdx')  # 4
ethy = make_frame('Ethylene.jdx', podg=0.3)  # 5
iso = make_frame('Isopropyl alcohol.jdx')  # 6
amm = make_frame('Ammonia.jdx')  # 7

clf = LogisticRegression(random_state=0, solver='lbfgs', multi_class='multinomial')
test_frame = pd.DataFrame(data=create_test_frame(ace, meth, acr, amm, ethy, iso, tric), columns=['X', 'y'])


X_train = test_frame['X']
X_train = X_train.values.reshape(-1, 1)
y_train = test_frame['y'].astype('float64')
clf.fit(X_train, y_train)
y_pred = clf.predict(X_train)
generate_matrix(y_train, y_pred)
plot_roc_curve(y_train, y_pred)



